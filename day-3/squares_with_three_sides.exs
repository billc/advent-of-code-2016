defmodule SquaresWithThreeSides do
  def calculate(input) do
    input
    |> parse
    |> Enum.count(&validTriangle/1)
  end

  def parse(triangles) do
    triangles
    |> String.split("\n")
    |> List.delete_at(-1)
    |> Enum.map(&split/1)
  end

  def split(triangle) do
    triangle
    |> String.trim
    |> String.split(~r/\s+/)
    |> Enum.map(&convert/1)
  end

  def convert(s) do
    case Integer.parse(s) do
      {n, _} -> n
      :error -> IO.puts("Input error #{s}")
    end
  end

  def validTriangle([a, b, c]), do: ((a + b) > c) && ((a + c) > b) && ((b + c) > a)
   
end

System.argv
|> hd
|> File.read!
|> SquaresWithThreeSides.calculate
# |> Enum.map(&IO.inspect/1)
|> IO.inspect
