defmodule VerticalSquares do
  def calculate(input) do
    input
    |> parse
    |> chunk_by_columns
    |> Enum.count(&validTriangle/1)
  end

  def parse(triangles) do
    triangles
    |> String.split("\n")
    |> List.delete_at(-1)
    |> Enum.map(&split/1)
  end

  def split(triangle) do
    triangle
    |> String.trim
    |> String.split(~r/\s+/)
    |> Enum.map(&convert/1)
  end

  def convert(s) do
    case Integer.parse(s) do
      {n, _} -> n
      :error -> IO.puts("Input error #{s}")
    end
  end

  def chunk_by_columns([ [a1, a2, a3], [b1, b2, b3], [c1, c2, c3] | tail ]) do
    [ [a1, b1, c1], [a2, b2, c2], [a3, b3, c3] | chunk_by_columns(tail) ]
  end

  def chunk_by_columns([]), do: []

  def validTriangle([a, b, c]), do: ((a + b) > c) and ((a + c) > b) and ((b + c) > a)
   
end

System.argv
|> hd
|> File.read!
|> VerticalSquares.calculate
# |> Enum.map(&IO.inspect/1)
|> IO.inspect
