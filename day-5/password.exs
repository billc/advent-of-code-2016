require Logger

defmodule Password do
  def calculate(input) do
    Stream.iterate(0, &(&1+1))
    |> Stream.filter(&validMD5(&1, input))
    |> Enum.take(8)
    |> Enum.map_join(&parse(&1, input))
  end

  def validMD5(i, s) do
    md5 = generate(i, s)
    {pre, _} = String.split_at(md5, 5) 
    pre == "00000"
  end

  def generate(i, s) do
    salt = s <> Integer.to_string(i)
    :crypto.hash(:md5 , salt) |> Base.encode16()
  end

  defp parse(i, s) do
    md5 = generate(i, s)
    String.at(md5, 5)
  end
end

System.argv
|> hd
|> Password.calculate
# |> Enum.map(&IO.inspect/1)
|> IO.inspect
