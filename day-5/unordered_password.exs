require Logger

defmodule UnorderedPassword do
  def calculate(input) do
    Agent.start_link(fn -> Map.new end, name: __MODULE__)

    Stream.iterate(0, &(&1+1))
    |> Stream.filter(&validMD5(&1, input))
    |> Enum.take_while(fn(i) ->
      generate(i, input)
      |> insertCode
      !passwordComplete?
      end
    )

    Enum.map_join(Agent.get(__MODULE__, &Map.values(&1)), "", &(&1))
  end

  def validMD5(i, s) do
    md5 = generate(i, s)
    {pre, _} = String.split_at(md5, 5) 
    pre == "00000"
  end

  def generate(i, s) do
    salt = s <> Integer.to_string(i)
    :crypto.hash(:md5 , salt) |> Base.encode16()
  end

  defp parse(i, s) do
    md5 = generate(i, s)
    String.at(md5, 5)
  end

  defp insertCode(md5) do
    position = String.at(md5, 5)
    value = String.at(md5, 6)

    if position >= "0" and position < "8" do
      Logger.debug position <> ": " <> value
      Agent.update(__MODULE__, &Map.put_new(&1, position, value))
    end
  end

  def passwordComplete?() do
    Agent.get(__MODULE__, &Map.keys(&1) == ["0", "1", "2", "3", "4", "5", "6", "7"])
  end
end

System.argv
|> hd
|> UnorderedPassword.calculate
# |> Enum.map_join(&IO.inspect/1)
|> IO.inspect
