defmodule Rooms do
  def sum_sector_ids(input) do
    input
    |> String.split("\n")
    |> List.delete_at(-1)
    |> Enum.map(&parse/1)
    # |> Enum.filter(&validate/1)
    # |> Enum.reduce(0, &calculate/2)
    |> Enum.map(&decode/1)
  end

  def parse(input) do
    s = input
    |> String.replace(["-", "[", "]"], " ")
    |> String.trim
    |> String.split(" ")

    [ id, checksum, ] = Enum.take(s, -2)
    room = s
    |> Enum.take(Enum.count(s) - 2)

    characters = room
    |> Enum.join
    |> String.codepoints

    [room, characters, String.to_integer(id), checksum]
  end

  def validate([_room, name, sector, checksum]) do
    result = name
    |> Enum.uniq
    |> Enum.sort(&(&1 < &2))
    |> Enum.reduce(%{},
    fn(x, acc) -> Map.put(acc, x, Enum.count(name, fn(y) -> y == x end)) end
    )
    |> Enum.sort_by(fn({k,v}) -> v end, &>=/2)
    |> Enum.take(5)
    |> Enum.map_join(fn({k,v}) -> k end)

    result == checksum
  end

  def calculate([_room, _name, sector, _checksum], acc) do
   sector + acc 
  end

  def decode([room, _name, sector, _checksum]) do
    shift_cipher = rem(sector, 26)
    s = room
    |> Enum.map_join(" ", &shift(&1, shift_cipher))
    [s, sector]
  end

  def shift(word,shift_cipher ) do
    word
    |> String.to_charlist
    |> Enum.map(fn(x) -> if x != 32, do: rem( x + shift_cipher ,122) + 96 * div((x + shift_cipher),122) end)
    
  end
  

end

System.argv
|> hd
|> File.read!
|> Rooms.sum_sector_ids
|> Enum.map(&IO.inspect/1)
# |> IO.inspect
