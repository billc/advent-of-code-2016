defmodule BathroomEnhanced do
  def calculate(input) do
    input
    |> parse
    |> Enum.reduce([{-2,0}], &decrypt/2)
    |> List.delete_at(at-1)
    |> Enum.map_join(&translate/1)
    |> String.reverse
  end

  def parse(directions) do
    directions
    # |> String.split(["\r\n", "\n"])
    |> String.split("\n")
    |> List.delete_at(-1)
    |> Enum.map(&String.codepoints/1)
  end

  def decrypt(cipher, [ current | _t ] = codes ) do
    code =
      cipher
      |> move(current)
    [code | codes]
  end

  def move([step | path], current) do
    move(path, walk(direction(step), current))
  end

  def move([], location) do
    location
  end

  def direction("U"), do: {0,1}
  def direction("D"), do: {0,-1}
  def direction("L"), do: {-1,0}
  def direction("R"), do: {1,0}

  def walk({dx, dy}, {x, y}) when abs(dx + x) + abs(dy + y) < 3, do: {dx + x, dy + y}
  def walk(_, current), do: current

  def translate(button) do
    panel = %{{0,0} => "7", {1,0} => "8", {2,0} => "9",
               {-1,1} => "2", {0,1} => "3", {1,1} => "4", 
              {0,2} => "1", {-2,0} => "5", {-1,0} => "6",
               {-1,-1} => "A", {0,-1} => "B", {1,-1} => "C",
               {0,-2} => "D"
              }
    panel[button]
  end

end

System.argv
|> hd
|> File.read!
|> BathroomEnhanced.calculate
|> IO.inspect
