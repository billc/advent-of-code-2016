defmodule BathroomSecurity do
  def calculate(input) do
    input
    |> parse
    |> Enum.reduce([{1,1}], &decrypt/2)
    |> List.delete_at(-1)
    |> Enum.map_join(&translate/1)
    |> String.reverse
  end

  def parse(directions) do
    directions
    |> String.split(["\r\n", "\n"])
    |> List.delete_at(-1)
    |> Enum.map(&String.codepoints/1)
  end

  def decrypt(cipher, [ last | _t ] = codes ) do
    code =
      cipher
      |> move(last)
    [code | codes]
  end

  def move([direction | path], last) do
    move(path, walk(direction, last))
  end

  def move([], location) do
    location
  end

  def walk("U", {x, y}) when y > 0, do: {x, y - 1}
  def walk("D", {x, y}) when y < 2, do: {x, y + 1}
  def walk("R", {x, y}) when x < 2, do: {x + 1, y}
  def walk("L", {x, y}) when x > 0, do: {x - 1, y}
  def walk(_, current), do: current

  def translate(button) do
    panel = %{{0,0} => 1, {1,0} => 2, {2,0} => 3,
               {0,1} => 4, {1,1} => 5, {2,1} => 6,
               {0,2} => 7, {1,2} => 8, {2,2} => 9
              }
    panel[button]
  end

end

System.argv
|> hd
|> File.read!
|> BathroomSecurity.calculate
|> IO.inspect
